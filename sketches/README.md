#Sketches for the Fathom WebApps

##Running Assessment View
*a view to see a running assessments (main action here is commit/reveal for assessors)*
sketches:
- AssApp (Julius)
- Main Frame (Antoine)

##Overview Dashboard
*a view to see potential, current/running and past assessments*
sketches :
- Overview Dashboard
