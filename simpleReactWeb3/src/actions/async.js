import Web3 from 'web3'
import { conceptRegistryABI } from '../ABIs.js'

export const WEB3_CONNECTED = 'WEB3_CONNECTED'
export const WEB3_DISCONNECTED = 'WEB3_DISCONNECTED'
export const RECEIVE_VARIABLE = 'RECEIVE_VARIABLE'

// actions to instantiate web3
export const web3Connect = () => {
  return (dispatch, getState) => {
    if (typeof window.web3 !== 'undefined') {
      let w3 = new Web3(window.web3.currentProvider)
      dispatch(web3Connected(w3))
      // after web3 is instanciated, fetch contract info (mew concept) and user address
      if (w3) {
        dispatch(fetchFathomParams())
        dispatch(fetchUserAddress())
      } else {
        dispatch(web3Disconnected())
      }
    }
  }
}

// action to save the connedted web3-instance in state
export function web3Connected (web3) {
  return {
    type: WEB3_CONNECTED,
    payload: {
      web3: web3,
      version: web3.version
    }
  }
}

// to save in state that one could not connect
export function web3Disconnected () {
  return {
    type: WEB3_DISCONNECTED,
    payload: {}
  }
}

export function fetchUserAddress () {
  return async (dispatch, getState) => {
    let w3 = getState().web3
    let accounts = await w3.eth.getAccounts()
    if (accounts.length === 0) {
      dispatch(receiveVariable('address', 'pleaseEnterPasswordToUnblockMetamask'))
    } else {
      dispatch(receiveVariable('address', accounts[0]))
    }
  }
}

// to save something from the chain in state
export function receiveVariable (name, value) {
  return {
    type: RECEIVE_VARIABLE,
    payload: {
      name: name,
      value: value
    }
  }
}

// reading all variables from the chain
export const fetchFathomParams = () => {
  return async (dispatch, getState) => {
    let w3 = getState().web3
    let cRegInstance = new w3.eth.Contract(conceptRegistryABI, getState().conceptRegistryAddress)
    let mew = await cRegInstance.methods.mewAddress().call()
    dispatch(receiveVariable('mewAddress', mew))
  }
}

export const actions = {
  web3Connect
}
