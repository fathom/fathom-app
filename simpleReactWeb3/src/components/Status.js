import { Component } from 'react'
var h = require('react-hyperscript')

export class Status extends Component {
  componentWillMount () {
    this.props.web3Connect()
  }

  render () {
    return (
      h('div', [
        h('div', [
          h('span', 'Web3-version: '),
          h('span', this.props.web3_version)
        ]),
        h('div', [
          h('span', 'user-address: '),
          h('span', this.props.address)
        ]),
        h('div', [
          h('span', 'ConceptRegistry: '),
          h('span', this.props.conceptRegistryAddress)
        ]),
        h('div', [
          h('span', 'Mew-Concept: '),
          h('span', this.props.mewAddress)
        ])
      ])
    )
  }
}

export default Status
