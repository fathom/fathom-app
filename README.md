## Simple React App

To see a simple React App that fetches web3 data, go into the simpleReactWeb3 folder and follow instructions.

We'll be using this as the basis for our different Front End components.

## Styleguide

We use the [JS-standard](https://standardjs.com/) styleguide. Use `npm run lint`
to lint the code and `npm run fix` to automatically fix the errors found.
