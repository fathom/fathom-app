`DISCUSSION` Proposal
<!---
A Template for Discussions...
The purpose of this template is to frame project discussions that fall outside other existing templates. 
Project discussion topics, like fruits, can be of different shapes, sizes, and degrees of ripeness. 
Hopefully this template will help us catch them all -- for consideration and collaborative response.
Topics that begin here may be repackaged into actionable issues once they've been given some thought and discussion. 

* If you are submitting a BUG report or FEATURE proposal instead, please use one of the other templates.
* Please provide a concise description of the discussion topic in the Title above
--->
--------------------------------------------------------------------------------

## Current Behavior
<!-- Describe the current behavior/state that makes this topic important to address -->


## Expected Behavior 
<!-- Describe the expected behavior/state that you hope to see following discussion/decisions/implementation -->


## Motivation and Context
<!-- Provide context (e.g., examples, use cases, user stories, goals) -->
<!-- How has this issue affected you? What are you trying to accomplish? -->


## Possible Implementation
<!-- Suggest a concrete way to explore this issue or a possible implementation -->
<!-- Include any details helpful to understanding what the implementation might entail or look like -->


## Info, Links, etc
<!-- Links: Post links to helpful info, data, resources -->
<!-- Images: Drag screenshots or other image files here -->
<!-- Code: Format any relevant code, commands, output, using code blocks (```) -->


## Notify Collaborators
<!-- Notify collaborators for this discussion by adding their GitLab handles here -->


--------------------------------------------------------------------------------

/label ~"discussion"

**Disclaimer:** This form is a work in progress. Feedback on its components is welcomed!

<!-- Add further specification using the drop-down lists below, as appropriate -->

