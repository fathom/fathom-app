`BUG` Report
<!--
* NOTE: This form is for reporting BUGS ONLY.
* If you are NOT reporting a bug please use a different issue template.
* Please provide a concise description of the Bug in the Title above.
--->
--------------------------------------------------------------------------------
## Current Behavior
<!-- What is the "Bug" behavior you are seeing? -->


## Expected Behavior 
<!-- What is the "Correct" behavior you should see instead? -->


## Possible Source of Bug
<!-- If you can, provide a link to the line of code that may be causing the problem -->


## Steps to Reproduce the Bug
<!-- How can the bug/problem be reproduced? (this is very important) -->
<!-- Provide a link to a live example, or an unambiguous set of steps to reproduce the bug -->
1.  
2.  
3.  


## Possible Solution
<!-- Suggest a bug fix, if possible --> 
<!-- Describe your proposed change and include any key implementation details -->


## Links, Screenshots, Logs
<!-- Links: Post links helpful to understanding the bug or designing a fix -->
<!-- Screenshots: Drag your screenshots or other image files here -->
<!-- Code: Format any relevant code/output/logs using code blocks (```) -->


## Notify Others
<!-- Notify relevant individuals by adding their GitLab handles here -->


--------------------------------------------------------------------------------
/label ~"bug"

<!-- Add further specification using the drop-down lists below, as appropriate -->

