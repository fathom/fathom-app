import { Component } from 'react'
var h = require('react-hyperscript')

export class CreateAssessment extends Component {
  constructor(props) {
    super(props);
    this.state={
      //creationStatus:""
    }
  }

  create(){
    this.props.createAssessmentFunction(this.props.conceptContract)
  }

  render () {
    //styles
    const frameStyle={style:{textAlign:"center",marginTop:"3em"}}
    const titleStyle={style:{fontSize:"1.8em",fontStyle:"bold"}}
    const buttonStyle={borderRadius:"0.8em",border:"1px solid black",margin:"1em 40%",padding:"0.2em 1em"}

    let status
    if (this.props.creationStatus==="waiting"){
      status=h('div',{style:{color:"orange"}},"waiting for assessment creation...")
    } else if (this.props.creationStatus==="success"){
      status=h('div',{style:{color:"green"}},"assessment created  !")
    } else {
      status=null
    } 

    if (this.props.conceptContract){
      if (this.props.address!==''){
        return h('div',frameStyle,[
          h('div',titleStyle,"Create an Assessment"),
          h('div',"this is where fields will be for custom assessments"),
          h('div',{style:buttonStyle,onClick:this.create.bind(this)},"Create Assessment"),
          status,
        ])
      } else {
        return h('div',frameStyle,'No Address loaded to deploy Assessment')
      }
    } else {
      return h('div',frameStyle,'No Concept loaded yet')
    }
  }
}

export default CreateAssessment
