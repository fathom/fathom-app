import { Component } from 'react'
var h = require('react-hyperscript')

export class ConceptList extends Component {

  loadConceptContractAndCreateAssessment(e){
    console.log(e.target.id)
    console.log(this.props.conceptAddressList[e.target.id])
    this.props.loadConceptContractAndCreateAssessment(this.props.conceptAddressList[e.target.id])
  }

  render() {
    //styles
    const titleStyle={style:{fontSize:"1.5em",margin:"2em 0",fontStyle:"bold"}}
    const mainFrameStyle={style:{textAlign:"center",border:"0.5px solid lightgrey",borderRadius:"0.5em",padding:"1em",margin:"1em"}}
    const conceptFrameStyle={style:{textAlign:"center",border:"0.5px solid lightgrey",borderRadius:"0.5em",margin:"1em 20%",paddingTop:"0.5em"}}
    const buttonStyle={padding:"0.2em",borderRadius:"0.8em",border:"1px solid black",margin:"1em 25%",padding:"0.2em 1em"}

    if (this.props.conceptAddressList.length===0){
      return null
    } else {
      return h('div',mainFrameStyle,[
        h('div','List of Concepts in this Concept Registry'),
        h('div',this.props.conceptAddressList.map((address,k)=>{
        const fieldStyle={style: {'fontSize': '1.2em','fontStyle':"bold"}}
        const valueStyle={style: {'fontSize': '0.9em'}}
          return h('div',conceptFrameStyle ,[
              h('div',fieldStyle, "Concept #"+k),
              h('div',valueStyle, address),
              h('div',{style:buttonStyle,onClick:this.loadConceptContractAndCreateAssessment.bind(this),id:k},"Create Assessment"),
          ])
        }))
      ])
    }
  }
}

export default ConceptList
