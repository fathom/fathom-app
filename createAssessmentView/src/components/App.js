import StatusBox from '../containers/StatusBox'
import LoadAndDisplayConceptBox from '../containers/LoadAndDisplayConceptBox'
import ConceptListBox from '../containers/ConceptListBox'
import CreateAssessmentBox from '../containers/CreateAssessmentBox'
var h = require('react-hyperscript')

const App = () => (
  h('div', [
    h(StatusBox),
    h(ConceptListBox),
    h(LoadAndDisplayConceptBox),
    h(CreateAssessmentBox)
  ])
)

export default App
