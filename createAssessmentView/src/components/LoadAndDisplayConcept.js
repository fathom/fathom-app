import { Component } from 'react'
var h = require('react-hyperscript')

export class LoadAndDisplayConcept extends Component {
  constructor(props) {
    super(props);
    this.state={
      conceptAddressInput:"",
    }
  }

  setconceptAddressInput(e){
    this.setState({conceptAddressInput:e.target.value})
  }


  isItConceptAddress (address) {
    let is= this.props.conceptAddressList.indexOf(address)
    return (is>-1)
  }

  loadConcept(){
    if (this.isItConceptAddress(this.state.conceptAddressInput)){
      this.props.loadConceptContract(this.state.conceptAddressInput)
    }
  }

  handleKeyPress(target) {
    if(target.charCode==13){
      this.loadConcept()
    }
  }


  render () {
    //styles
    const frameStyle={style:{textAlign:"center",paddingTop:"4em"}}
    const buttonStyle={padding:"0.2em",borderRadius:"0.8em",border:"1px solid black",margin:"1em 40%",padding:"0.2em 1em"}
    const conceptLoadedStyle={style:{fontSize:"0.9em"}}
    //validator
    const isAddress=h('div',{style:{color:"green",fontSize:"0.7em",padding:"0.2em"}},'is a Concept address')
    const isNotAddress=h('div',{style:{color:"red",fontSize:"0.7em",padding:"0.2em"}},'is not a Concept address')
    let validator
    let conceptAddressInput=this.state.conceptAddressInput
    if (conceptAddressInput.length===0){
      validator=null
    } else if (this.isItConceptAddress(conceptAddressInput)){
      validator=isAddress
    } else{
      validator=isNotAddress
    }
    return (
      h('div',frameStyle, [
        h('div', "Enter Concept Address"),
        h('input', {type:"text",onChange:this.setconceptAddressInput.bind(this), onKeyPress:this.handleKeyPress.bind(this)}),
        validator,
        h('div',{style:buttonStyle,onClick:this.loadConcept.bind(this)},"Load Concept"),
        h('div',conceptLoadedStyle, [
          h('span','Concept loaded : '),
          h('span',this.props.conceptAddress)
        ])
      ])
    )
  }
}

export default LoadAndDisplayConcept
