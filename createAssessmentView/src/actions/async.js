import Web3 from 'web3'
import { conceptRegistryABI,conceptABI } from '../ABIs.js'

export const WEB3_CONNECTED = 'WEB3_CONNECTED'
export const WEB3_DISCONNECTED = 'WEB3_DISCONNECTED'
export const RECEIVE_VARIABLE = 'RECEIVE_VARIABLE'


// actions to instantiate web3
export const web3Connect = () => {
  return (dispatch, getState) => {
    if (typeof window.web3 !== 'undefined') {
      let w3 = new Web3(window.web3.currentProvider)
      dispatch(web3Connected(w3))
      // after web3 is instanciated, fetch contract info (mew concept) and user address
      if (w3) {
        dispatch(fetchFathomParams())
        dispatch(fetchUserAddress())
      } else {
        dispatch(web3Disconnected())
      }
    }
  }
}

// action to save the connedted web3-instance in state
export function web3Connected (web3) {
  return {
    type: WEB3_CONNECTED,
    payload: {
      web3: web3,
      version: web3.version
    }
  }
}

// to save in state that one could not connect
export function web3Disconnected () {
  return {
    type: WEB3_DISCONNECTED,
    payload: {}
  }
}

export function fetchUserAddress () {
  return async (dispatch, getState) => {
    let w3 = getState().web3
    let accounts = await w3.eth.getAccounts()
    if (accounts.length === 0) {
      dispatch(receiveVariable('address', 'pleaseEnterPasswordToUnblockMetamask'))
    } else {
      dispatch(receiveVariable('address', accounts[0]))
    }
  }
}

export function loadConceptContract (address) {
  return async (dispatch, getState) => {
    let w3 = getState().web3
    let contractInstance

    try{
      contractInstance=await new w3.eth.Contract(conceptABI,address)
    } catch (e){
      console.log("was not a contract address")
      dispatch(receiveVariable("conceptAddress","This isn't a Concept Address from this Concept Registry"))
    }
    if (contractInstance){
      dispatch(receiveVariable("conceptAddress",address))
      dispatch(receiveVariable("conceptContract",contractInstance))
    }
  }
}

export function createAssessmentFunction (contract) {
  return async (dispatch, getState) => {
    let address = getState().address
    //define constants for assessments  
    const cost = 10;
    const size = 5;
    const endTime = 1000000000000;
    const startTime = 1000000000;
    const assesseeAddress=address
    dispatch(receiveVariable("creationStatus","waiting"))
    let tx=await contract.methods.makeAssessment(cost, size, startTime, endTime).send({from:assesseeAddress,gas: 3200000}) 
    
    dispatch(receiveVariable("creationStatus","success"))
  }
}

//combination of two functions above for directly creating assessments from conceptList
export function loadConceptContractAndCreateAssessment (address) {
  return async (dispatch, getState) => {
    let w3 = getState().web3
    let contractInstance

    try{
      contractInstance=await new w3.eth.Contract(conceptABI,address)
    } catch (e){
      console.log("was not a contract address")
      dispatch(receiveVariable("conceptAddress","This isn't a Concept Address from this Concept Registry"))
    }
    if (contractInstance){
      dispatch(receiveVariable("conceptAddress",address))
      dispatch(receiveVariable("conceptContract",contractInstance))
      let address = getState().address
      //define constants for assessments  
      const cost = 10;
      const size = 5;
      const endTime = 1000000000000;
      const startTime = 1000000000;
      const assesseeAddress=address
      dispatch(receiveVariable("creationStatus","waiting"))
      let tx=await contractInstance.methods.makeAssessment(cost, size, startTime, endTime).send({from:assesseeAddress,gas: 3200000}) 
      
      dispatch(receiveVariable("creationStatus","success"))
    }
  }
}


// to save something from the chain in state
export function receiveVariable (name, value) {
  return {
    type: RECEIVE_VARIABLE,
    payload: {
      name: name,
      value: value
    }
  }
}

// reading all variables from the chain
export const fetchFathomParams = () => {
  return async (dispatch, getState) => {
    let w3 = getState().web3
    let cRegInstance = new w3.eth.Contract(conceptRegistryABI, getState().conceptRegistryAddress)
    let mew = await cRegInstance.methods.mewAddress().call()
    dispatch(receiveVariable('mewAddress', mew))
    dispatch(listConcepts(cRegInstance))
  }
}

export const listConcepts = (cRegInstance) => {
  return async (dispatch, getState) => {
    let w3 = getState().web3
    //use events to list concept adresses
    let events=await cRegInstance.getPastEvents({fromBlock: 0,toBlock:"latest"})
    let listOfAdresses=events.map((e)=>{
      console.log(e)
      return e.returnValues._concept
    })
    dispatch(receiveVariable('conceptAddressList', listOfAdresses))
  }
}

export const actions = {
  web3Connect
}
