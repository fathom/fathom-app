import { connect } from 'react-redux'
import LoadAndDisplayConcept from '../components/LoadAndDisplayConcept'
import { loadConceptContract } from '../actions/async.js'

const mapStateToProps = state => {
  return {
    conceptAddress: state.conceptAddress,
    conceptAddressList:state.conceptAddressList
  }
}

const mapDispatchToProps = {
  loadConceptContract
}

export default connect(mapStateToProps, mapDispatchToProps)(LoadAndDisplayConcept)
