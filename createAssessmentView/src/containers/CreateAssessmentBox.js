import { connect } from 'react-redux'
import CreateAssessment from '../components/CreateAssessment'
import { createAssessmentFunction } from '../actions/async.js'

const mapStateToProps = state => {
  return {
    conceptContract: state.conceptContract,
    conceptAddress: state.conceptAddress,
    address:state.address,
    creationStatus:state.creationStatus
  }
}

const mapDispatchToProps = {
  createAssessmentFunction,
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateAssessment)
