import { connect } from 'react-redux'
import Assessments from '../components/Assessments'

const mapStateToProps = state => {
    return {
      assessmentList: state.assessmentList,
    }
}

export default connect(mapStateToProps)(Assessments)
