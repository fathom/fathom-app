import { connect } from 'react-redux'
import Status from '../components/Status'
import { web3Connect, fetchFathomParams,fetchUserAddress } from '../actions/async.js'

const mapStateToProps = state => {
    return {
      block: state.block,
      web3_version: state.web3_version,
      mewAddress: state.mewAddress,
      ConceptRegistry: state.ConceptRegistry,
      address:state.address,
      networkID:state.networkID,
      balance:state.balance
    }
}

const mapDispatchToProps = {
  web3Connect,
  fetchFathomParams,
  fetchUserAddress
}

export default connect(mapStateToProps, mapDispatchToProps)(Status)
