import React, { Component } from 'react'
import StatusBox from '../containers/StatusBox'
import AssessmentsBox from '../containers/AssessmentsBox'
var h = require('react-hyperscript')

const App = () => (
	h('div',[
	  h(StatusBox),
	  h(AssessmentsBox)
	])
)

export default App
