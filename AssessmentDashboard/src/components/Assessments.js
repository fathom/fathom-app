import React, { Component } from 'react'
import PropTypes from 'prop-types'
var h = require('react-hyperscript')

export class Assessments extends Component {

  componentWillMount() {
    //this.props.web3Connect()
  }

  render() {
    if (this.props.assessmentList.length===0){
      return null
    } else {
      return h('div',this.props.assessmentList.map((as,k)=>{
        const titleStyle={style: {'fontSize': '24px','fontStyle':"bold"}}
        const fieldStyle={style: {'fontSize': '14px','fontStyle':"bold"}}
        const valueStyle={style: {'fontSize': '11px'}}
          return h('div',[
              h('br'),
              h('div',titleStyle, "Assessment #"+k),
              h('br'),
              h('div',fieldStyle, "address : "),
              h('div',valueStyle, as.address),
              h('br'),
              h('div',fieldStyle, "cost : "),
              h('div',valueStyle, as.cost),
              h('br'),
              h('div',fieldStyle, "size : "),
              h('div',valueStyle, as.size),
              h('br'),
              h('div',fieldStyle, "assessee : "),
              h('div',valueStyle, as.assessee),
              h('br'),
              h('div',fieldStyle, "stage : "),
              h('div',valueStyle, as.stage),
              h('br'),
              h('div',fieldStyle, "conceptAddress : "),
              h('div',valueStyle, as.conceptAddress),
              h('br'),
              h('div',fieldStyle, "conceptData : "),
              h('div',valueStyle, as.conceptData),
              h('br'),
          ])
      }))
    }
  }
}

export default Assessments
