import React, { Component } from 'react'
import PropTypes from 'prop-types'
var h = require('react-hyperscript')

export class Notifications extends Component {

  componentWillMount() {
    //this.props.web3Connect()
  }

  render() {
    if (this.props.notifications.length===0){
      return null
    } else {
      return h('div',this.props.notifications.map((notif,k)=>{
        if (notif.returnValues.sender){
          return h('div',[
              h('br'),
              h('div',{style: {'font-size': '20','font-style':"bold"}}, "Notification"),
              h('div', "sender (concept) : "),
              h('br'),
              h('div', notif.returnValues.sender),
              h('div', "user : "),
              h('br'),
              h('div', notif.returnValues.user),
              h('div', "topic : "),
              h('br'),
              h('div', notif.returnValues.topic)
          ])
        } else{
          return h('div',[
              h('br'),
              h('div',{style: {'font-size': '20','font-style':"bold"}}, "Notification"),
              h('div', '"no sender field')
          ])
        }
      }))
    }
  }
}

export default Notifications
