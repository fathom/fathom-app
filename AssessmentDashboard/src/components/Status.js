import React, { Component } from 'react'
import PropTypes from 'prop-types'
var h = require('react-hyperscript')

export class Status extends Component {

  componentWillMount() {
    this.props.web3Connect()
  }

  render() {
    const NetworkNames={
      4:'rinkeby',
      1:'mainnet',
      3:'ropsten',
      42:'kovan'
    }
    if (NetworkNames[this.props.networkID]){
      var network=NetworkNames[this.props.networkID]
    } else {
      var network="Local or unknown"
    }
    console.log(network)
    return (
      h('div', [
        h('div',[
          h('span', "Web3-version: "),
          h('span', this.props.web3_version)
        ]),
        h('div',[
          h('span', "Network: "),
          h('span', network)
        ]),
        h('div',[
          h('span', "user-address: "),
          h('span', this.props.address)
        ]),
        h('div',[
          h('span', "AHA balance: "),
          h('span', this.props.balance)
        ]),
        h('div',[
          h('span', 'ConceptRegistry: '),
          h('span', this.props.ConceptRegistry._address),
        ]),
        h('div',[
          h('span', 'Mew-Concept: '),
          h('span', this.props.mewAddress)
        ])
      ])
    )
  }
}

Status.propTypes = {
  web3Connect:  React.PropTypes.func.isRequired,
  web3_version: React.PropTypes.string.isRequired,
  mewAddress: React.PropTypes.string.isRequired,
  address: React.PropTypes.string.isRequired,
}

export default Status
